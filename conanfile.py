from conans import ConanFile, CMake, tools
import os

class RemoteryConan(ConanFile):
    name = "Remotery"
    version = "master"
    license = "Apache-2.0"
    author = "David Yip <yipdw@member.fsf.org>"
    url = "https://gitlab.peach-bun.com/conan-packages/remotery"
    homepage = "https://github.com/Celtoys/Remotery"
    description = "Single C file, Realtime CPU/GPU Profiler with Remote Web Viewer"
    topics = ("profiling")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "CMakeLists.txt", "bin/*"
    options = {
        "enabled": [True, False],
        "fPIC": [True, False],
        "use_cuda": [True, False],
        "use_d3d11": [True, False],
        "use_metal": [True, False],
        "use_opengl": [True, False],
    }
    default_options = {
        "enabled": True,
        "fPIC": False,
        "use_cuda": False,
        "use_d3d11": False,
        "use_metal": False,
        "use_opengl": False,
    }

    def source(self):
        git = tools.Git(folder="Remotery")
        git.clone("https://github.com/Celtoys/Remotery.git", shallow=True)

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC
        cmake.definitions["RMT_ENABLED"] = self.options.enabled
        cmake.definitions["RMT_USE_CUDA"] = self.options.use_cuda
        cmake.definitions["RMT_USE_D3D11"] = self.options.use_d3d11
        cmake.definitions["RMT_USE_METAL"] = self.options.use_metal
        cmake.definitions["RMT_USE_OPENGL"] = self.options.use_opengl

        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include/Remotery", src="Remotery/lib")
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*", dst="lib/vis", src="Remotery/vis")
        self.copy("*", dst="bin", src="bin")

    def package_info(self):
        self.cpp_info.libs = ["Remotery"]
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))

        if self.settings.os == "Windows":
            self.cpp_info.libs += ["ws2_32"]
        elif self.settings.os == "Linux":
            self.cpp_info.libs += ["pthread", "m"]
        
        if not self.options.enabled:
            self.cpp_info.defines += ["RMT_ENABLED=0"]

    def configure(self):
        del self.settings.compiler.libcxx
