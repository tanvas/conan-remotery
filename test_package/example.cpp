#include <iostream>
#include <Remotery/Remotery.h>

int main() {
	Remotery* rmt = nullptr;
	rmt_CreateGlobalInstance(&rmt);
	rmt_DestroyGlobalInstance(rmt);
	return 0;
}
